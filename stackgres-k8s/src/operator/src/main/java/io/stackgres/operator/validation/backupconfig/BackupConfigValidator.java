/*
 * Copyright (C) 2019 OnGres, Inc.
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

package io.stackgres.operator.validation.backupconfig;

import io.stackgres.operator.validation.BackupConfigReview;
import io.stackgres.operatorframework.Validator;

public interface BackupConfigValidator extends Validator<BackupConfigReview> {

}
