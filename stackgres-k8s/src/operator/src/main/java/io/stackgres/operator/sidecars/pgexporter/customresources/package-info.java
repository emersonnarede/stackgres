/*
 * Copyright (C) 2019 OnGres, Inc.
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/**
 * Custom Resource for postgres exporter configuration parameters.
 */

package io.stackgres.operator.sidecars.pgexporter.customresources;
