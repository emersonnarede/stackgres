/*
 * Copyright (C) 2019 OnGres, Inc.
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

package io.stackgres.operator.validation.profile;

import io.stackgres.operator.validation.SgProfileReview;
import io.stackgres.operatorframework.Validator;

interface SgProfileValidator extends Validator<SgProfileReview> {
}
