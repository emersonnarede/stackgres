# GraalVM Quarkus image
image: docker.io/ongres/ubi-graalvm-maven:19.2.1

variables:
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: |
               -Dhttps.protocols=TLSv1.2
               -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
               -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
               -Dorg.slf4j.simpleLogger.showDateTime=true
               -XX:+TieredCompilation
               -XX:TieredStopAtLevel=1
               -Djava.awt.headless=true
               -XX:+UseParallelOldGC
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: |
                   -f stackgres-k8s/src/pom.xml
                   --batch-mode
                   --errors
                   --fail-at-end
                   --show-version
                   -DinstallAtEnd=true
                   -DdeployAtEnd=true

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_NAME"'
cache:
  paths:
    - .m2/repository

stages:
  - compile
  - package
  - pre test
  - test
  - check-release
  - deploy

compile:
  stage: compile
  tags:
    - stackgres-runner
  script:
    - 'mvn $MAVEN_CLI_OPTS clean verify -P safer'
  artifacts:
    paths:
      - stackgres-k8s/src/target
      - stackgres-k8s/src/*/target

native image:
  stage: package
  tags:
    - stackgres-runner
  dependencies:
    - compile
  script:
    - MAVEN_CLI_OPTS="$MAVEN_CLI_OPTS -DskipTests"
    - 'mvn $MAVEN_CLI_OPTS package -P native'
    - 'cp $GRAALVM_HOME/jre/lib/amd64/libsunec.so stackgres-k8s/src/operator/target/'
    - 'cp $GRAALVM_HOME/jre/lib/security/cacerts stackgres-k8s/src/operator/target/'
  artifacts:
    paths:
      - stackgres-k8s/src/operator/target/libsunec.so
      - stackgres-k8s/src/operator/target/cacerts
      - stackgres-k8s/src/operator/target/stackgres-operator-runner
      - stackgres-k8s/src/operator/target/lib/
      - stackgres-k8s/src/operator/target/stackgres-operator-runner.jar
    expire_in: 1 day

enforce release:
  stage: check-release
  tags:
    - stackgres-runner
  dependencies:
    - compile
  script:
    - MAVEN_CLI_OPTS="$MAVEN_CLI_OPTS -DskipTests -Dquarkus.skipAugmentation=true"
    - 'mvn $MAVEN_CLI_OPTS enforcer:enforce@enforce-no-snapshots'
  only:
    - tags

build test images:
  stage: pre test
  tags:
    - stackgres-runner
  dependencies:
    - compile
  script:
    - MAVEN_CLI_OPTS="$MAVEN_CLI_OPTS -DskipTests -Dquarkus.skipAugmentation=true"
    - 'mvn $MAVEN_CLI_OPTS pre-integration-test -P integration'

build jvm image:
  stage: pre test
  tags:
    - stackgres-runner
  dependencies:
    - compile
  script:
    - 'export IMAGE_TAG="${CI_COMMIT_TAG:-"$CI_COMMIT_REF_NAME"}"-jvm'
    - 'export IMAGE_NAME="stackgres/operator:$IMAGE_TAG"'
    - MAVEN_CLI_OPTS="$MAVEN_CLI_OPTS -DskipTests"
    - 'mvn $MAVEN_CLI_OPTS package -P build-image-jvm'

build native image:
  stage: pre test
  tags:
    - stackgres-runner
  dependencies:
    - compile
    - native image
  script:
    - 'export IMAGE_TAG="${CI_COMMIT_TAG:-"$CI_COMMIT_REF_NAME"}"'
    - 'export IMAGE_NAME="stackgres/operator:$IMAGE_TAG"'
    - MAVEN_CLI_OPTS="$MAVEN_CLI_OPTS -DskipTests"
    - 'mvn $MAVEN_CLI_OPTS package -P build-image-native'

reset kind:
  stage: pre test
  tags:
    - stackgres-runner
  variables:
    KIND_SUFFIX: "none"
  script:
    - MAVEN_CLI_OPTS="$MAVEN_CLI_OPTS -DskipTests -Dquarkus.skipAugmentation=true"
    - 'flock -w 3600 /tmp/stackgres-integration-test mvn $MAVEN_CLI_OPTS verify -P reset-kind'
  only:
    variables:
      - $RESET_KIND

reset kind jvm image:
  stage: pre test
  tags:
    - stackgres-runner
  variables:
    KIND_SUFFIX: jvm-image
  script:
    - MAVEN_CLI_OPTS="$MAVEN_CLI_OPTS -DskipTests -Dquarkus.skipAugmentation=true"
    - 'flock -w 3600 /tmp/stackgres-integration-test-jvm-image mvn $MAVEN_CLI_OPTS verify -P reset-kind'
  only:
    variables:
      - $RESET_KIND

reset kind native image:
  stage: pre test
  tags:
    - stackgres-runner
  variables:
    KIND_SUFFIX: native-image
  script:
    - MAVEN_CLI_OPTS="$MAVEN_CLI_OPTS -DskipTests -Dquarkus.skipAugmentation=true"
    - 'flock -w 3600 /tmp/stackgres-integration-test-native-image mvn $MAVEN_CLI_OPTS verify -P reset-kind'
  only:
    variables:
      - $RESET_KIND

integration test:
  stage: test
  tags:
    - stackgres-runner
  dependencies:
    - compile
  retry: 2
  script:
    - 'flock -w 3600 /tmp/stackgres-integration-test mvn $MAVEN_CLI_OPTS verify -P integration'

integration test jvm image:
  stage: test
  tags:
    - stackgres-runner
  dependencies:
    - compile
  variables:
    OPERATOR_IN_KUBERNETES: "true"
    KIND_SUFFIX: jvm-image
  retry: 2
  script:
    - 'export IMAGE_TAG="${CI_COMMIT_TAG:-"$CI_COMMIT_REF_NAME"}"-jvm'
    - 'export IMAGE_NAME="stackgres/operator:$IMAGE_TAG"'
    - 'flock -w 3600 /tmp/stackgres-integration-test-jvm-image mvn $MAVEN_CLI_OPTS verify -P integration -DargLine=-Dcom.ongres.junit.docker.runnerId="$KIND_SUFFIX"'

integration test native image:
  stage: test
  tags:
    - stackgres-runner
  dependencies:
    - compile
  variables:
    OPERATOR_IN_KUBERNETES: "true"
    KIND_SUFFIX: native-image
  script:
    - 'export IMAGE_TAG="${CI_COMMIT_TAG:-"$CI_COMMIT_REF_NAME"}"'
    - 'export IMAGE_NAME="stackgres/operator:$IMAGE_TAG"'
    - 'flock -w 3600 /tmp/stackgres-integration-test-native-image mvn $MAVEN_CLI_OPTS verify -P integration -DargLine=-Dcom.ongres.junit.docker.runnerId="$KIND_SUFFIX"'

deploy jvm image:
  image: docker.io/ongres/builder:latest
  stage: deploy
  tags:
    - stackgres-runner
  script:
    - 'export IMAGE_TAG="${CI_COMMIT_TAG:-"$CI_COMMIT_REF_NAME"}"-jvm'
    - 'export IMAGE_NAME="stackgres/operator:$IMAGE_TAG"'
    - 'buildah pull docker-daemon:"$IMAGE_NAME"'
    - 'buildah push --format=docker --authfile $REGISTRY_AUTH_FILE $IMAGE_NAME "docker://docker.io/$IMAGE_NAME"'
  only:
    variables:
      - $CI_COMMIT_REF_NAME == "development"
      - $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^latest-.*$/
      - $DO_DEPLOY

deploy native image:
  image: docker.io/ongres/builder:latest
  stage: deploy
  tags:
    - stackgres-runner
  script:
    - 'export IMAGE_TAG="${CI_COMMIT_TAG:-"$CI_COMMIT_REF_NAME"}"'
    - 'export IMAGE_NAME="stackgres/operator:$IMAGE_TAG"'
    - 'buildah pull docker-daemon:"$IMAGE_NAME"'
    - 'buildah push --format=docker --authfile $REGISTRY_AUTH_FILE $IMAGE_NAME "docker://docker.io/$IMAGE_NAME"'
  only:
    variables:
      - $CI_COMMIT_REF_NAME == "development"
      - $CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^latest-.*$/
      - $DO_DEPLOY
