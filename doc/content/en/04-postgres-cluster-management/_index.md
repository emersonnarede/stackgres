---
title: Postgres cluster management
weight: 4
pre: "<b>4. </b>"
chapter: true
---

### Chapter 4

# Postgres cluster management

Management of postgres clusters created with StackGres.
